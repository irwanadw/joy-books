var express = require('express');
var router = express.Router();
const booksController = require('../controllers/books')

/* GET users listing. */
router.get('/', booksController.getListBookBySubjects);
router.post('/', booksController.pickupBook);
router.get('/:bookCode', booksController.getDetailBook);

module.exports = router;
