const axios = require('axios');

class BooksController { 
    static async getListBookBySubjects (req,res ) {
        try {
            const { subject, details } = req.query;
            const listBookBySubject = await axios.get(`https://openlibrary.org/subjects/${subject}.json`, {
                params: {
                    ...(details ? { details }: {}),
                }
            });
            return res.status(200).json({
                status: "Success",
                data: listBookBySubject.data
            })
            
        } catch (error) {
            console.log("error", error)
            res.status(500).json(error);
        }
    }

    static async getDetailBook (req,res ) {
        try {
            const { bookCode } = req.params;
            console.log(bookCode)
            const bookDetail = await axios.get(`https://openlibrary.org/authors/${bookCode}.json`);
            return res.status(200).json({
                status: "Success",
                data: bookDetail.data
            })
            
        } catch (error) {
            console.log("error", error)
            res.status(500).json(error);
        }
    }

    static async pickupBook (req, res){
        try {
            const { pickupTime, bookCode } = req.body ;
            const bookDetail = await axios.get(`https://openlibrary.org/books/${bookCode}.json`); 
            const authorList =  bookDetail.data.authors
            const authorKey =  authorList[0].author.key
            const authorDetail = await axios.get(`https://openlibrary.org${authorKey}.json`);
           
            return res.status(201).json({
                status: "Pickup Success",
                data: {
                    pickupTime: new Date(pickupTime),
                    title: bookDetail.data.title,
                    author: authorDetail.data.name,
                    edition: bookDetail.data.latest_revision,

                }
            })

        } catch (error) {
            console.log("error", error)
            res.status(500).json(error);
        }
    }
}

module.exports = BooksController